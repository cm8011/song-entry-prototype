(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  project.catalogSub = (function () {
    function getUserCatalogs () {
      return JSON.parse(window.localStorage.getItem('user-catalogs')) || [];
    }

    function getActiveCatalog () {
      return JSON.parse(window.localStorage.getItem('active-catalog'));
    }

    function setUserCatalogs (catalogs) {
      window.localStorage.setItem('user-catalogs', JSON.stringify(catalogs));
    }

    function setActiveCatalog (catalog) {
      window.localStorage.setItem('active-catalog', JSON.stringify(catalog));
    }

    function addUserCatalog (catalog) {
      var catalogs = getUserCatalogs();
      catalogs.push(catalog);
      setUserCatalogs(catalogs);
    }

    return {
      getUserCatalogs: getUserCatalogs,
      getActiveCatalog: getActiveCatalog,
      setActiveCatalog: setActiveCatalog,
      addUserCatalog: addUserCatalog
    };
  })();
})();
