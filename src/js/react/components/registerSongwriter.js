/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.RegisterSongwriter = React.createClass({
    getInitialState: function () {
      return {
        disabled: true
      };
    },
    handleClick: function (e) {
      e.preventDefault();
      var songwriters = project.songwriterSub.get();
      var index;
      var name = $('#songwriter-first-name').val() + ' ' + $('#songwriter-last-name').val();

      if (
        songwriters[songwriters.length - 1] === '' ||
        !project.songwriterValidator.isValidSongwriter(songwriters[songwriters.length - 1])
        ) {
        index = songwriters.length - 1;
      } else {
        index = songwriters.length;
      }
      console.log(index);
      project.songwriterSub.addAvailableSongwriter(name);
      project.songwriterSub.addSongwriter(index, name);
      toastr['success']('Successfully registered songwriter: ' + name);
      project.emitter.emit('refresh-songwriters');
      project.emitter.emit('check-fields');
      project.emitter.emit('song-details-update');
      // window.location.reload();
    },
    enableSubmit: function () {
      var firstName = $('#songwriter-first-name').val() ? true : false;
      var lastName =  $('#songwriter-last-name').val() ? true : false;
      var bool = firstName && lastName;
      this.setState({ disabled: !bool });
    },
    render: function () {
      return (
        <fieldset className='song-contributors'>
          <legend>Register Songwriter</legend>
          <div className='row'>
            <div className='medium-8 large-6 columns'>
              <div className='row collapse'>
                <div className='small-10 columns'>
                    <react.TextInput
                      label='First Name'
                      onChange={this.enableSubmit}
                      id='songwriter-first-name'
                      placeholder='First Name' />

                    <react.TextInput
                      label='Last Name'
                      onChange={this.enableSubmit}
                      id='songwriter-last-name'
                      placeholder='Last Name' />

                  <button
                    disabled={this.state.disabled}
                    className='button success'
                    id='register-songwriter'
                    onClick={this.handleClick}>
                    Register Songwriter
                  </button>
                </div>
              </div>
            </div>
          </div>
        </fieldset>
      );
    }
  });
})();
