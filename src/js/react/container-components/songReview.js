/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.SongReview = React.createClass({
    getInitialState: function () {
      return {
        activeCatalog: project.catalogSub.getActiveCatalog(),
        loading: false,
        fieldsFull: false,
        songSessionDetails: project.songDetailsSub.getSongDetails(),
        songwriters: project.songwriterSub.get(),
        songSessionLyrics: project.songLyricsSub.get()
      };
    },
    getSongData: function () {
      return {
        catalog: this.state.activeCatalog || {},
        details: this.state.songSessionDetails|| {},
        lyrics: this.state.songSessionLyrics || {},
        songwriters: this.state.songwriters || []
      };
    },
    render: function () {
      var songData = this.getSongData();
      return (
        <div>
          <h3><i className='fa fa-list-alt'></i>Almost done!  Review and register the song.</h3>
          <p>To make sure the information you`re about to provide is accurate, please take a close look at the details of your song.  When you are sure everything looks okay, go ahead and select <strong>Register Song</strong>.</p>

          <react.SongOverview songData={songData} showEdit={true} />
          <react.SumbitSong songData={songData} />

        </div>
      );
    }
  });

  react.SongOverview = React.createClass({
    showEdit: function () {
      var editLinks = {
        catalog: null,
        songDetails: null,
        lyrics: null
      };
      if (this.props.showEdit === true) {
        editLinks.catalog = <a className='edit' href={project.paths.songEntry}>Edit catalog</a>;
        editLinks.songDetails = <a className='edit' href={project.paths.songEntry + '/song-details'}>Edit song details</a>;
        editLinks.lyrics = <a className='edit' href={project.paths.songEntry + '/lyrics'}>Edit song lyrics</a>;
      }
      return editLinks;
    },
    render: function () {
      var songData = this.props.songData;
      var editLinks = this.showEdit();
      return (
        <div className='song-entry-form'>
          <form>
             <fieldset>
              <legend>Catalog</legend>
              <label>Catalog Name</label>
              <span>{songData.catalog.catalogName}</span>
              {editLinks.catalog}
            </fieldset>
             <fieldset>
              <legend>Song Details</legend>
              <label>Song title</label> {songData.details.songTitle}
              <label>Copyright year</label> {songData.details.songYear}
              {editLinks.songDetails}
            </fieldset>
            <react.SongwritersReview songwriters={songData.songwriters} showEdit={this.props.showEdit} />
            <fieldset>
              <legend>Song Lyrics</legend>
              <label>Song language</label> {songData.lyrics.language}
              <label>Lyrics</label>
              <p>{songData.lyrics.lyrics}</p>
              {editLinks.lyrics}
            </fieldset>
          </form>
        </div>
      );
    }
  });

  react.SongwritersReview = React.createClass({
    getInitialState: function () {
      return {
        songwriters: project.songwriterSub.get()
      }
    },
    renderSongwriters: function () {
      var html = [];
      this.state.songwriters.map(function (item, i) {
        var songwriter =
        <div>
          <span>{item}</span>
          <br />
        </div>
          ;
        html.push(songwriter);
      });
      return html;
    },
    showEdit: function () {
      if (this.props.showEdit === true) {
        return <a className='edit' href={project.paths.songEntry + '/song-details'}>Edit songwriters</a>;
      }
    },
    render: function () {
      return (
        <fieldset>
          <legend>Songwriters</legend>
          <label>Name</label>
          {this.renderSongwriters()}
          {this.showEdit()}
        </fieldset>

      );
    }
  });

  react.NoSongData = React.createClass({
    render: function () {
      return (
        <div className='song-entry-form'>
          <form>
             <fieldset>
              <p>This page has expired.</p>
            </fieldset>
          </form>
        </div>
      );
    }
  });

  react.SumbitSong = React.createClass({
    parseSongData: function () {
      var data = {};
      var sessionData = this.props.songData;

      if (sessionData !== null) {
        data.songCatalog = sessionData.catalog;
        data.details = sessionData.details;
        data.lyrics = sessionData.lyrics;
        return data;
      } else {
        console.log('Error with song data');
      }
    },
    handleSubmit: function (e) {
      e.preventDefault();
      // var data = this.parseSongData();
      // project.dataService.postNewSong({
      //   data: data,
      //   success: function (data) {
      //     console.log('SUCCESSFUL SONG POST TO API');

      //   }
      // });
      project.dataService.simulateLoadTime()
      .then(function() {
        window.location.href = project.paths.songEntry + '/done';
      });
    },
    render: function () {
      return (
        <div className='song-entry-actions'>
          <a className='secondary-action'>Save &amp; Exit</a>
          <a className='secondary-action' href={project.paths.songEntry + '/lyrics'}>Go back</a>
          <a onClick={this.handleSubmit} className='button success' href={project.paths.songEntry + '/done'}>Register Song</a>
        </div>
      );
    }
  });

  var selector = $('[data-react="song-review"]');
  if (selector.length) {
    selector.each(function () {
      ReactDOM.render(
        <react.SongReview />
        , $(this)[0]
      );
    });
  }
})();
