/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.SongDetails = React.createClass({
    getInitialState: function () {
      return {
        fieldsFull: false,
        songSessionDetails: project.songDetailsSub.getSongDetails(),
        hasValidSongwriters: false
      };
    },
    componentDidMount: function () {
      project.emitter.on('check-fields', this.checkFields);
      project.emitter.on('song-details-update', this.updateSongDetails);
      this.checkFields();
    },
    updateSongDetails: function () {
      this.setState({ songSessionDetails: project.songDetailsSub.getSongDetails() });
      this.checkFields();
    },
    checkFields: function () {
      var self = this;
      self.hasValidSongwriters()
      .then(function (result) {
        if (
            self.state.songSessionDetails.songTitle.length &&
            self.state.songSessionDetails.songYear.length &&
            result
            ) {
          self.setState({ fieldsFull: true });
        } else {
          self.setState({ fieldsFull: false });
        }
      });
    },
    saveAction: function () {
      project.songDetailsSub.setSongDetails(this.state.songSessionDetails);
    },
    hasValidSongwriters: function () {
      var self = this;
      return new Promise(function (resolve, reject) {
        var result = true;
        var addedSongwriters = project.songwriterSub.get();
        addedSongwriters.forEach(function (item) {
          var isValid = project.songwriterValidator.isValidSongwriter(item);
          if (!isValid) {
            result = false;
          }
        });
        self.setState({ hasValidSongwriters: result });
        resolve(result);
      });
    },
    handleChange: function (e) {
      var details = this.state.songSessionDetails;
      details[e.target.getAttribute('data-key')] = e.target.value;
      project.songDetailsSub.setSongDetails(details);
      project.emitter.emit('song-details-update');
    },
    render: function () {
      return (
        <div>
          <h3><i className='fa fa-music'></i>Enter song details.</h3>
          <div className='song-entry-form'>
            <div className='song-entry-htmlForm'>
              <form>
                <fieldset className='song-contributors'>
                  <legend>Song Details</legend>
                  <div className='row'>
                    <div className='large-6 columns'>
                      <react.TextInput
                        onChange={this.handleChange}
                        dataKey='songTitle'
                        label='Song Title'
                        id='song-title'
                        value={this.state.songSessionDetails.songTitle}
                        placeholder='Enter the song title'/>
                    </div>

                    <div className='large-3 columns end'>
                      <react.TextInput
                        onChange={this.handleChange}
                        dataKey='songYear'
                        label='Copyright year'
                        id='copyright-year'
                        value={this.state.songSessionDetails.songYear}
                        maxLength='4'
                        placeholder='Ex. 1999' />
                    </div>
                  </div>
                </fieldset>
                <react.SongWriters hasValidSongWriters={this.state.hasValidSongwriters} />
            </form>
          </div>
        </div>
        <react.FormButtons
          saveAction={this.saveAction}
          data={this.state.songSessionDetails}
          nextPage={project.paths.songEntry + '/lyrics'}
          prevPage={project.paths.songEntry}
          nextEnabled={this.state.fieldsFull} />
      </div>
      );
    }
  });

  var selector = $('[data-react="song-details"]');
  if (selector.length) {
    selector.each(function () {
      ReactDOM.render(
        <react.SongDetails />
        , $(this)[0]
      );
    });
  }
})();
