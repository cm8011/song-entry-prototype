var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var minifyCss = require('gulp-minify-css');
var plumber = require('gulp-plumber');
var react = require('gulp-react');
var rename = require('gulp-rename');
var shell = require('gulp-shell');
var streamqueue = require('streamqueue');
var uglify = require('gulp-uglify');

process.gulp = {};

var config = require('./config');

var files = {
  toCopy: 'src/copy/**/*',
  dest: {
    baseDir: 'build',
    jsDir: 'build/assets/js'
  },
  watch: {
    toReload: [
      'build/**/*.css',
      'build/**/*.html',
      'build/**/*.js'
    ],
    toCopy: ['src/copy/*'],
    toCompile: {
      sass: 'src/sass/**/*.scss',
      hbs: 'src/views/**/*.hbs',
      js: 'src/js/**/*.js'
    }
  },
  src: {
    sass: 'src/sass/index.scss',
    jsBaseDir: 'src/js',
    jsRequire: [
      'node_modules/jquery/dist/jquery.js',
      'src/js/vendor/jquery/jquery-ui.js',
      'src/js/vendor/modernizr/modernizr-custom.js',
      'node_modules/lodash/lodash.js',
      'src/sass/vendor/foundation/js/foundation.js',
      'node_modules/handlebars/dist/handlebars.js',
      'node_modules/react/dist/react-with-addons.js',
      'node_modules/eventemitter2/lib/eventemitter2.js',
      'node_modules/react-dom/dist/react-dom.js',
      'node_modules/toastr/toastr.js'
    ],
    jsMain: [
      'src/js/open.js',
      'src/js/main.js',
      'src/js/util/util.js',
      'src/js/util/songwriterUtil.js',
      'src/js/dataService.js',
      'src/js/data-handlers/catalog.js',
      'src/js/data-handlers/details.js',
      'src/js/data-handlers/songwriters.js',
      'src/js/json/songwriterDB.js',
      'src/js/react/components/secondaryActionWIcon.js',
      'src/js/react/components/textInput.js',
      'src/js/react/components/alertBar.js',
      'src/js/react/components/formButtons.js',
      'src/js/react/components/catalogModal.js',
      'src/js/react/components/createCatalog.js',
      'src/js/react/components/songwriter.js',
      'src/js/react/components/registerSongwriter.js',
      'src/js/react/components/existingCatalogs.js',
      'src/js/react/components/catalogForm.js',
      'src/js/react/container-components/selectCatalog.js',
      'src/js/react/container-components/songwriters.js',
      'src/js/react/container-components/songDetails.js',
      'src/js/react/container-components/songLyrics.js',
      'src/js/react/container-components/songReview.js',
      'src/js/react/container-components/done.js'
    ],
    jsDefer: [
      'src/js/defer.js'
    ],
    html: [
      'build/**/*.html'
    ]
  },
  build: {
    css: 'build/assets/css'
  }
};

gulp.task('beta', function () {
  gulp.start('distsass');
  gulp.start('distJs');
  gulp.start('copyAll');
  gulp.start('distHtml');
});

gulp.task('build', function () {
  gulp.start('buildsass');
  gulp.start('buildJs');
  gulp.start('copyAll');
  gulp.start('nodeBuild');
});

gulp.task('buildJs', function () {
  // main
  streamqueue({ objectMode: true },
    gulp.src(files.src.jsRequire),
    gulp.src(files.src.jsMain).pipe(react())
  )
  .pipe(concat('index.js'))
  .pipe(gulp.dest(files.dest.jsDir));

  // defer
  gulp.src(files.src.jsDefer)
  .pipe(concat('defer.js'))
  .pipe(gulp.dest(files.dest.jsDir));
});

gulp.task('distJs', function () {
  // main
  streamqueue({ objectMode: true },
    gulp.src(files.src.jsRequire),
    gulp.src(files.src.jsMain)
    .pipe(react())
  )
  .pipe(concat('index.js'))
  .pipe(uglify())
  .pipe(gulp.dest(files.dest.jsDir));

  // defer
  gulp.src(files.src.jsDefer)
  .pipe(concat('defer.js'))
  .pipe(uglify())
  .pipe(gulp.dest(files.dest.jsDir));
});

gulp.task('distHtml', ['nodeBeta'], function () {
  var attr = { // https://github.com/kangax/html-minifier
    removeComments: true,
    removeCommentsFromCDATA: true,
    removeCDATASectionsFromCDATA: true,
    collapseWhitespace: true,
    conservativeCollapse: false,
    preserveLineBreaks: false,
    collapseBooleanAttributes: false,
    removeAttributeQuotes: false,
    removeRedundantAttributes: false,
    preventAttributesEscaping: false,
    useShortDoctype: false,
    removeEmptyAttributes: false,
    removeScriptTypeAttributes: false,
    removeStyleLinkTypeAttributes: false,
    removeOptionalTags: false,
    removeIgnored: false,
    removeEmptyElements: false,
    lint: false,
    keepClosingSlash: true,
    caseSensitive: false,
    minifyJS: true,
    minifyCSS: true,
    minifyURLs: false,
    ignoreCustomComments: [],
    ignoreCustomFragments: [],
    processScripts: [],
    maxLineLength: 0,
    customAttrAssign: [],
    customAttrSurround: [],
    customAttrCollapse: null,
    quoteCharacter: '"'
  };
  gulp.src(files.src.html)
  .pipe(htmlmin(attr))
  .pipe(gulp.dest(files.dest.baseDir));
});

gulp.task('buildsass', function () {
  return gulp.src(files.src.sass)
    .pipe(plumber({
      errorHandler: function (err) {
        console.log(err);
        this.emit('end');
      }
    }))
    .pipe(sass())
    .pipe(rename(function (path) {
      path.basename;
    }))
    .pipe(gulp.dest(files.build.css));
});

gulp.task('distsass', function () {
  return gulp.src(files.src.sass)
    .pipe(plumber({
      errorHandler: function (err) {
        console.log(err);
        this.emit('end');
      }
    }))
    .pipe(sass())
    .pipe(minifyCss())
    .pipe(rename(function (path) {
      path.basename;
    }))
    .pipe(gulp.dest(files.build.css));
});

gulp.task('copyAll', function () {
  gulp.src(files.toCopy).pipe(gulp.dest(files.dest.baseDir));
});

gulp.task('copyTouched', function () {
  var srcFile = process.gulp.livereload.path.substr(__dirname.length + 1);
  var destDir = files.dest.baseDir + srcFile.substr(3 + String('/copy').length);
  destDir = destDir.substr(0, destDir.lastIndexOf('/'));
  gulp.src(srcFile).pipe(gulp.dest(destDir));
});

gulp.task('nodeBeta', shell.task(['node lib/build.js']));
gulp.task('nodeBuild', shell.task(['node lib/build.js']));

// gulp.task('nodeBeta', shell.task(['VERSION=' + config.public.VERSION + ' NODE_ENV=beta node lib/build']));
// gulp.task('nodeBuild', shell.task(['VERSION=' + config.public.VERSION + ' node lib/build']));

gulp.task('watch', function () {
  livereload.listen(35729);

  try {
    gulp.watch(files.watch.toReload, function (evt) {
      setTimeout(function () {
        livereload.changed(evt.path);
        browserSync.reload();
      }, 500);
    });

    gulp.watch([
      files.watch.toCopy
    ], function (evt) {
      process.gulp.livereload = evt;
      return gulp.start('copyTouched');
    });

    gulp.watch([files.watch.toCompile.sass], ['buildsass'], browserSync.reload());
    gulp.watch([files.watch.toCompile.js], ['buildJs'], browserSync.reload());
    gulp.watch([files.watch.toCompile.hbs], ['nodeBuild'], browserSync.reload());
  } catch (e) {
    console.log(e);
  }
});

gulp.task('default', ['build'], function () {
  setTimeout(function () {
    console.log('Watching ..');
    return gulp.start('watch');
  }, 500);
  browserSync.init({
    server: {
      baseDir: './build/'
    }
  });
});
