/* global project */

(function () {
  'use strict';

  project.dataService = {
    simulateLoadTime: (function () {
      var loadTime = 300; // ms

      function load () {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            resolve();
          }, loadTime);
        });
      }

      return load;
    })(),
    postNewCatalogLocal: (function () {
      function postData (catalog) {
        var data = JSON.parse(window.localStorage.getItem('user-catalogs'));
        if (data === undefined || data.length === 0) {
          data = [];
          data.push(catalog);
        } else {
          data.push(catalog);
        }
        window.localStorage.setItem('user-catalogs', JSON.stringify(data));
      }

      return postData;
    })(),
    getUserData: (function () {
      var endpoint = '/getUserData';

      function success (data) {
        console.log('AJAX SUCCESS');
        console.log(data);
      }

      function getData (attrs) {
        project.emitter.emit('api', {
          endpoint: endpoint,
          success: function (data) {
            attrs.success(data);
          }
        });
      }

      return getData;
    })(),
    postNewCatalog: (function () {
      var endpoint = '/postNewCatalog';

      function postData (attrs) {
        var data = { name: attrs.name || 'default' };
        project.emitter.emit('api', {
          endpoint: endpoint,
          data: data,
          method: 'post',
          success: function (data) {
            if (typeof attrs.success === 'function') {
              attrs.success();
            }
          }
        });
      }

      return postData;
    })(),
    postNewSong: (function () {
      var endpoint = '/postNewSong';

      function postData (attrs) {
        var data = { data: attrs.data || null };
        project.emitter.emit('api', {
          endpoint: endpoint,
          data: data,
          method: 'post',
          success: function (data) {
            if (typeof attrs.success === 'function') {
              attrs.success();
            }
          }
        });
      }

      return postData;
    })()    
  };
})();
