# Song Entry Prototype

### To run project
- If you don't have it, install [nodejs](https://nodejs.org/en/download/)
- You may need to update npm in if you are in a windows environment (npm -v > 3.0.0)
```bash
npm update -g
```

##### Serve Client Locally
```bash
npm install
gulp
```