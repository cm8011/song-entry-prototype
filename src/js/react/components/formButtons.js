/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;
  react.FormButtons = React.createClass({
    handleSave: function (e) {
      e.preventDefault();
      window.location.href = project.paths.songEntry + '/song-entry-overview';
    },
    handleBack: function (e) {
      e.preventDefault();
      window.location.href = project.paths.songEntry + '/song-entry-overview';
    },
    handleNext: function (e) {
      e.preventDefault();
      if (this.props.nextEnabled) {
        if (this.props.saveAction) {
          this.props.saveAction();
        }
        window.location = this.props.nextPage;
      }
    },
    render: function () {
      var disabled = this.props.nextEnabled ? '' : ' disabled';
      return (
        <div className='song-entry-actions'>
          <a onClick={this.handleSave} className='secondary-action'>Save &amp; Exit</a>
          <a onClick={this.handleBack} className='secondary-action'>Cancel</a>
          <a onClick={this.handleNext} className={'button' + disabled}>Save &amp; Continue</a>
        </div>
      );
    }
  });
})();
