/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.SelectCatalog = React.createClass({
    getInitialState: function () {
      return {
        catalogs: project.catalogSub.getUserCatalogs(),
        activeCatalog: project.catalogSub.getActiveCatalog(),
        isCreationMode: false
      };
    },
    componentWillMount: function () {
      project.emitter.on('catalog-refresh', this.refreshState);
      this.setState({ isCreationMode: this.state.catalogs.length === 0 });
    },
    refreshState: function () {
      this.setCatalogState();
      this.setState({ isCreationMode: false });
    },
    setCatalogState: function () {
      this.setState({ catalogs: project.catalogSub.getUserCatalogs() });
      this.setState({ activeCatalog: project.catalogSub.getActiveCatalog() });
    },
    checkFields: function () {
      return project.catalogSub.getActiveCatalog() && !this.state.isCreationMode;
    },
    toggleMode: function () {
      this.setState({isCreationMode: !this.state.isCreationMode});
    },
    handleChange: function (e) {
      project.catalogSub.setActiveCatalog(JSON.parse(e.target.value));
      this.setCatalogState();
    },
    render: function () {
      var isCreationMode = this.state.isCreationMode;
      var creationAction =
        <react.SecondaryActionWIcon
          leadingText='Need a brand new catalog for this song?'
          onClick={this.toggleMode}
          icon='fa fa-plus-circle'
          text='Create New Catalog' />;
      var existingAction =
        <react.SecondaryActionWIcon
          onClick={this.toggleMode}
          icon='fa fa-arrow-circle-left'
          text='Use existing catalog' />;
      var catalogForm =
        <react.CatalogForm
          activeCatalog={this.state.activeCatalog}
          handleChange={this.handleChange}
          catalogs={this.state.catalogs} />;
      return (
        <div>
          <h3><i className='fa fa-folder-open' ></i>{isCreationMode ? 'Register' : 'Select'} a catalog. {<react.CatalogModal />}</h3>
          <div className='song-entry-form'>
            {isCreationMode ? <react.CreateCatalog /> : catalogForm}
            {isCreationMode ? existingAction : creationAction}
          </div>
          <react.FormButtons
            nextPage={project.paths.songEntry + '/song-details'}
            prevPage={project.paths.songEntry}
            nextEnabled={this.checkFields()} />
        </div>
			);
    }
  });

  var selector = $('[data-react="register"]');
  if (selector.length) {
    selector.each(function () {
      ReactDOM.render(
        <react.SelectCatalog />
        , $(this)[0]
      );
    });
  }
})();
