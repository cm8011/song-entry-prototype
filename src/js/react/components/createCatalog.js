/* global React */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.CreateCatalog = React.createClass({
    handleSubmit: function (e) {
      e.preventDefault();
      var catalog = { catalogName: $('#catalog-name').val(), createNew: true };
      project.catalogSub.addUserCatalog(catalog);
      project.catalogSub.setActiveCatalog(catalog);
      project.emitter.emit('catalog-refresh');
      toastr.success('Successfully registered catalog: ' + catalog.catalogName);
      // window.location.reload();
    },
    render: function () {
      return (
        <div>
          <form className='add-new-catalog'>
            <div className='row'>
              <div className='medium-10 large-8 columns'>
                <div className='row collapse'>
                  <div className='small-12 columns'>
                    <label>
                    Catalog Name
                    <input
                      className='input-alert'
                      required
                      type='text'
                      id='catalog-name'
                      placeholder='' />
                      </label>
                      <p className='input-subtext'>e.g. John Smith`s Songbook or First Church of Christ`s Songbook</p>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <button onClick={this.handleSubmit} className='button success'>Register Catalog</button>
        </div>
      );
    }
  });
})();
