// TODO: Split up into seperate files
module.exports = (function () {
  'use strict';

  process.env.NODE_ENV = process.env.NODE_ENV || 'local';

  var config = {
    public: {},
    private: {}
  };

  // Version
  config.public.VERSION = process.env.VERSION || getVersion();

  return config;
})();

function getVersion () {
  var currentdate = new Date();
  return currentdate.getDate() + '_' +
    (currentdate.getMonth() + 1) + '_' +
    currentdate.getFullYear() + '_' +
    currentdate.getHours() + '_' +
    currentdate.getMinutes() + '_' +
    currentdate.getSeconds();
}
