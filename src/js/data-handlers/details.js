/* global $, React, ReactDOM */

(function () {
  'use strict';

  $('body').disableSelection();

  var project = window.project;
  var react = project.react;

  project.songDetailsSub = (function () {
    function setSongDetails (songDetails) {
      window.localStorage.setItem('song-details', JSON.stringify(songDetails));
    }

    function getSongDetails () {
      return JSON.parse(window.localStorage.getItem('song-details')) || '';
    }

    function init () {
      if (!getSongDetails()) {
        setSongDetails({
          songTitle: '',
          songYear: '',
        });
      }
    }

    init();

    return {
      setSongDetails: setSongDetails,
      getSongDetails: getSongDetails
    };
  })();
})();
