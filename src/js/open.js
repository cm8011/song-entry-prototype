/* global  $, EventEmitter2, _ */
(function () {
  'use strict';

  var project = window.project = window.project || {};
  project.state = project.state || {};
  // console.log('v' + window.project.context.config.VERSION);

  var react = project.react = {};

  project.emitter = new EventEmitter2();

  project.emitter.onAny(function (event, value) {
    console.log('EVENT', event, value || '');
  });

  project.emitter.on('api', apiHandler);
  project.emitter.on('error', handleError);

  $(document).ready(function () {
    project.emitter.emit('$ready');
  });

  function apiHandler (attrs) {
    // var api = 'https://my-prototyper-api.herokuapp.com';
    var api = 'http://localhost:3010';
    var method = attrs.method || 'get';
    var data = {
      type: method,
      url: api + attrs.endpoint,
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      cache: attrs.cache || true,
      beforeSend: function (xhr) {
        if (_.result(project, 'state.user')) {
          try {
            xhr.setRequestHeader(
              'Authorization', 'Bearer ' + project.state.user.token
            );
          } catch (e) {
            project.emitter.emit('error', e);
          }
        }
      },
      success: function (data) {
        try {
          (attrs.success)
          ? attrs.success(data)
          : attrs.self.setState({data: data});
        } catch (e) {}
      },
      error: function (xhr, status, err) {
        if (attrs.hasOwnProperty('error')) return attrs.error(xhr, status, err);
        project.emitter.emit('error', err);
      }
    };
    if (attrs.data) {
      data.data = method === 'get' ? attrs.data : JSON.stringify(attrs.data);
    }
    $.ajax(data);
  }

  function handleError (err) {
    console.log('fn:handleError');
    console.log(err);
  }
})();
