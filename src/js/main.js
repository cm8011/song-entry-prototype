/* global $, project */

(function () {
  // Variable Paths
  project.paths = {};
  project.paths.songEntry = '/song-entry';

  // Get AJAX Request Sample
  $('#get-data-button').on('click', function (e) {
    e.preventDefault();
    $('#movie-mock-data').html('');
    project.emitter.emit('api', {
      endpoint: '/getData',
      success: function (response) {
        $('#movie-mock-data').html(JSON.stringify(response.data.catalogs[0]));
      }
    });
  });

  // Post AJAX Request Sample
  $('#add-catalog').on('click', function (e) {
    var data = {};
    data.name = $('#catalog-name').val();
    e.preventDefault();
    project.emitter.emit('api', {
      endpoint: '/postData',
      method: 'post',
      data: data,
      success: function (response) {
        $('#post-message').html(
        '<p class="text-center alert alert-warning">' + response.message + '</p>'
        );
        $(".select-catalog").prepend('<span class="catalog-item"><input type="radio" name="catalog" value="'+data.name+'" id="'+data.name+'" checked><label for="'+data.name+'">'+data.name+'</label></span>');
        $("#catalog-name").val("");
        $(".no-catalogs-yet").hide();
        $(".select-catalog").show();
        $(".song-entry-actions a.button").removeClass("disabled");
      }
    });
  });

  $('#clean-state').on('click', function () {
    window.localStorage.clear();
    window.location.reload();
  });

    $('#clean-catalogs').on('click', function () {
    window.localStorage.removeItem('active-catalog');
    window.localStorage.removeItem('user-catalogs');
    window.location.reload();
  });

  $('#clean-added-songwriters').on('click', function () {
    window.localStorage.removeItem('added-songwriters');
    window.location.reload();
  });

  $('#clean-available-songwriters').on('click', function () {
    window.localStorage.removeItem('available-songwriters');
    window.location.reload();
  });

  $('#add-available-songwriters').on('click', function () {
    project.songwriterSub.setAvailableSongwriters();
    window.location.reload();
  });

  $('#loading').on('click', function () {
    $(".loading-overlay").fadeIn().delay(3000).fadeOut();
  });

})();
