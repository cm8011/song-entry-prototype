/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.SongDone = React.createClass({
    getInitialState: function () {
      return {
        activeCatalog: project.catalogSub.getActiveCatalog(),
        loading: false,
        fieldsFull: false,
        songSessionDetails: project.songDetailsSub.getSongDetails(),
        songSessionLyrics: project.songLyricsSub.get(),
        songwriters: project.songwriterSub.get()
      };
    },
    componentDidMount: function () {
      this.clearSongSession();
    },
    clearSongSession: function () {
      console.log('CLEARING SONG DATA');
      window.localStorage.removeItem('song-details');
      window.localStorage.removeItem('song-lyrics');
      window.localStorage.removeItem('added-songwriters');
    },
    getSongData: function () {
      return {
        catalog: this.state.activeCatalog || {},
        details: this.state.songSessionDetails || {},
        lyrics: this.state.songSessionLyrics || {},
        songwriters: this.state.songwriters || []
      }
    },
    successMessage: function () {
      return (
        <div> 
          <h3><i className='fa fa-check'></i>The song was registered successfully!</h3>
          <p>We've processed your song and everything looks good!  You may want to take note of the details below by printing this page.  We've also emailed a copy of this to your primary email address.</p>
          <p className='song-number'><strong>CCLI Song Number</strong> 12345678</p>
        </div>
      );
    },
    render: function () {
      var songData = this.getSongData();
      return (
        <div>

          {songData ? this.successMessage() : null}

          <div>
            {songData ? <react.SongOverview songData={songData} showEdit={false} /> : <react.NoSongData />}
            <react.SongDoneButtons hidePrint={songData === null} />

          </div>
        </div>
      );
    }
  });

  react.SongDoneButtons = React.createClass({
    printButton: function () {
      if (!this.props.hidePrint) {
        return <a className='secondary-action'><i className='fa fa-print'></i>Print this page</a>;
      }
    },
    render: function () {
      return (
        <div className='song-entry-actions'>
          {this.printButton()}
          <a className='button success' href={project.paths.songEntry}>Add Another Song</a>
        </div>
      );
    }
  });

  var selector = $('[data-react="song-done"]');
  if (selector.length) {
    selector.each(function () {
      ReactDOM.render(
        <react.SongDone />
        , $(this)[0]
      );
    });
  }
})();
