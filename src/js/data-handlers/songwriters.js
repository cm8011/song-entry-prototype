/* global $ */

(function () {
  'use strict';

  var project = window.project;
  // project.CCLISongwriters = ['Ruben Morgan', 'Darlene Zschech', 'Kari Jobe', 'Franklin Tarter', 'Martin Smith', 'John Adams', 'Zeek', 'Adam Smith'];
  project.songwriterSub = (function () {

    // Private
    function initSongwriters () {
      var availableSongwriters = getAvailableSongwriters();
      if (!availableSongwriters) {
        setAvailableSongwriters(['']);
      }
      var addedSongwriters = getAddedSongwriters();
      if (!addedSongwriters) {
        updateAddedSongwriters(['']);
      }
    }

    // Public
    function updateAddedSongwriters (data) {
      window.localStorage.setItem('added-songwriters', JSON.stringify(data));
    }

    function setAvailableSongwriters (songwriters) {
      if (!songwriters) {
        return null;
      }
      window.localStorage.setItem('available-songwriters', JSON.stringify(songwriters));
    }

    function removeSongwriter (index, songwriter) {
      var addedSongwriters = getAddedSongwriters();
      addedSongwriters.splice(index, 1);
      updateAddedSongwriters(addedSongwriters);
    }

    function addSongwriter (index, songwriter) {
      var addedSongwriters = getAddedSongwriters();
      addedSongwriters.splice(index, 1, songwriter);
      updateAddedSongwriters(addedSongwriters);
    }

    function getAddedSongwriters () {
      return JSON.parse(window.localStorage.getItem('added-songwriters'));
    }

    function addEmptySongwriter () {
      var songwriters = JSON.parse(window.localStorage.getItem('added-songwriters'));
      songwriters.push('');
      window.localStorage.setItem('added-songwriters', JSON.stringify(songwriters));
    }

    function getAvailableSongwriters () {
      var songwriters = JSON.parse(window.localStorage.getItem('available-songwriters'));
      return songwriters ? songwriters : [];
    }

    function addAvailableSongwriter (songwriter) {
      var songwriters = getAvailableSongwriters();
      songwriters.push(songwriter);
      setAvailableSongwriters(songwriters);
    }

    function getAddedSongwriterByIndex (index) {
      var addedSongwriters = getAddedSongwriters();
      return addedSongwriters[index];
    }

    initSongwriters();

    return {
      get: getAddedSongwriters,
      addSongwriter: addSongwriter,
      removeSongwriter: removeSongwriter,
      addEmptySongwriter: addEmptySongwriter,
      getAvailableSongwriters: getAvailableSongwriters,
      setAvailableSongwriters: setAvailableSongwriters,
      addAvailableSongwriter: addAvailableSongwriter,
      getAddedSongwriterByIndex: getAddedSongwriterByIndex
    };
  })();
})();
