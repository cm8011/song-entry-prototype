/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.SongWriters = React.createClass({
    getInitialState: function () {
      return {
        songwriters: project.songwriterSub.get(),
        isCreationMode: false
      };
    },
    componentDidMount: function () {
      project.emitter.on('refresh-added-songwriters', this.refreshSongwriters);
      project.emitter.on('refresh-songwriters', this.resetState);
    },
    resetState: function () {
      this.refreshSongwriters();
      this.setState({ isCreationMode: false });
    },
    refreshSongwriters: function () {
      var songwriters = project.songwriterSub.get();
      this.setState({ songwriters: songwriters });
    },
    renderSongWriters: function () {
      var self = this;
      var html = [];
      this.state.songwriters.map(function (item, i) {
        var partial =
          <react.SongWriter
            value={item}
            key={i} id={i}
            isValidSongwriter={project.songwriterValidator.isValidSongwriter(item)} />;
        html.push(partial);
      });
      return html;
    },
    showAlert: function () {
      if (!this.props.hasValidSongWriters && !this.state.isCreationMode) {
        for (var i = 0; i < this.state.songwriters.length; i++) {
          if (!this.state.songwriters[i].trim()) {
            return false;
          }
        }
        return true;
      } else {
        return false;
      }
    },
    addSongwriter: function (e) {
      e.preventDefault();
      project.songwriterSub.addEmptySongwriter();
      project.emitter.emit('refresh-added-songwriters');
      project.emitter.emit('check-fields');
    },
    toggleMode: function () {
      this.setState({isCreationMode: !this.state.isCreationMode});
      project.emitter.emit('check-fields');
    },
    setCreationMode: function () {
      this.setState({isCreationMode: true});
    },
    switchToExistingMode: function () {
        return <p>
                  <a className='secondary-action' onClick={this.toggleMode}>
                    <i className='fa fa-arrow-circle-left' aria-hidden='true'></i>
                    Use Existing Songwriter
                  </a>
                </p>;
    },
    switchToCreationMode: function () {
      return <p className='subtle-text'>
              Can`t find the songwriter you`re looking for?
              <a className='secondary-action save-icon' onClick={this.toggleMode}>
                <br />
                <i className='fa fa-pencil-square-o' aria-hidden='true'></i>
                Register New Songwriter
              </a>
            </p>;
    },
    addSongwriterButton: function () {
      return <p className='subtle-text'>
        <a className='secondary-action' onClick={this.addSongwriter}>
          <i className='fa fa-plus-circle' aria-hidden='true'></i>
          Add another songwriter
        </a>
      </p>;
    },
    render: function () {
      var alertIcon = <i className='fa fa-exclamation-circle' aria-hidden='true'></i>;
      return (
        <div>
          <fieldset className='song-contributors'>
            <legend>Song Contributors</legend>
            <div className='row'>
              <label className='medium-6 columns'>Songwriter</label>
              <label className='medium-6 columns'>Contribution</label>
            </div>
            <div className='song-contributors-list'>
              {this.renderSongWriters()}
            </div>

            {this.showAlert() ? <react.AlertBar
              message='No songwriters found registered with that name. Click to register a new songwriter'
              type='warning'
              icon={alertIcon}
              onClick={this.setCreationMode} /> : null}

            {this.state.isCreationMode ? <react.RegisterSongwriter /> : null}

            {!this.state.isCreationMode ? this.addSongwriterButton() : null}
          </fieldset>
            {this.state.isCreationMode ? this.switchToExistingMode() : this.switchToCreationMode()}
        </div>
      );
    }
  });
})();
