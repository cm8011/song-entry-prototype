/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.SongWriter = React.createClass({
    componentDidMount: function () {
      this.bindAutocomplete();
      $('#songwriter-' + this.props.id).on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          e.preventDefault();
          return false;
        }
      });
    },
    bindAutocomplete: function () {
      var self = this;
      var input = $('#songwriter-' + this.props.id).autocomplete({
        minLength: 0,
        source: function (request, response) {
          var data = project.songwriterSub.getAvailableSongwriters();
          project.songwriterValidator.filterSongwriters(request.term, data)
          .then(function(filteredList) {
            if (filteredList.length !== 0) {
              response(filteredList);
            } else {
              project.songwriterValidator.filterSongwriters(request.term, project.CCLISongwriters)
              .then(function (filteredDB) {
                response(filteredDB);
              });
            }
          });
        },
        select: function (e, ui) {
          e.preventDefault();
          input.val(ui.item.value);
          self.handleChange(e);
        }
      }).on('focus', function () {
        $(this).autocomplete('search', input.val());
      });
    },
    handleChange: function (e) {
      console.log('CHANGE EVENT', e.target.value, this.props.id);
      var val = e.target.value;
      project.songwriterSub.addSongwriter(this.props.id, val);
      project.emitter.emit('refresh-added-songwriters');
      project.emitter.emit('check-fields');
    },
    handleRemove: function () {
      if (project.songwriterSub.get().length > 1) {
        var val = $('#songwriter-' + this.props.id).val();
        project.songwriterSub.removeSongwriter(this.props.id, val);
        project.emitter.emit('refresh-added-songwriters');
        project.emitter.emit('check-fields');
      }
    },
    render: function () {
      var className = this.props.isValidSongwriter ? ' songwriter-success' : 'songwriter-not-found';
      var id = this.props.id;
      return (
        <div>
          <div className='row song-contributor'>
            <div className='medium-6 columns'>
              <input
                value={this.props.value}
                onChange={this.handleChange}
                placeholder='Begin typing to select songwriter'
                type='text'
                className={className}
                id={'songwriter-' + id} />
            </div>
            <div className='medium-6 columns'>
              <input
                id={'word-contribution-' + id}
                type='checkbox'
                defaultChecked />
                <label htmlFor={'word-contribution-' + id}>Lyrics</label>
              <input
                id={'music-contribution-' + id}
                type='checkbox'
                defaultChecked />
              <label htmlFor={'music-contribution-' + id}>Music</label>
              <span
                className='alert'
                onClick={this.handleRemove}>&nbsp;<i className='fa fa-minus-circle remove-icon' aria-hidden='true'></i>
              </span>
            </div>
          </div>
        </div>
      );
    }
  });
})();
