/* global $ */

(function () {
  'use strict';

  var project = window.project;
  project.songwriterValidator = (function () {
    // function validateSongwriters (songwriters) {
    //   if (!songwriters || songwriters.length === 0) {
    //     return false;
    //   }
    //   for (var songwriter in songwriters) {
    //     if (!isValidSongwriter(songwriter)) {
    //       return false;
    //     }
    //   }
    //   return true;
    // }

    function validateSongwriters (songwriters) {
      return new Promise(function (resolve, reject) {
        var result = true;
        var addedSongwriters = project.songwriterSub.get();
        addedSongwriters.forEach(function (item) {
          var isValid = project.songwriterValidator.isValidSongwriter(item);
          if (!isValid) {
            result = false;
          }
        });
        resolve(result);
      });
    }

    function isValidSongwriter (val) {
      var result = true;
      if (
        !isWriterInArray(val, project.songwriterSub.getAvailableSongwriters()) &&
        !isWriterInArray(val, project.CCLISongwriters)
      ) {
        result = false;
      } else if (val === '') {
        result = false;
      }
      return result;
    }

    function isWriterInArray (item, array) {
      if (Array.isArray(array)) {
        if (array.indexOf(item) !== -1) {
          return true;
        } else {
          return false;
        }
      }
    }

    function filterSongwriters (term, array) {
        return new Promise(function (resolve, reject) {
          var regex = new RegExp(term, 'i');
          var filteredItems = [];
          for (var i = 0; i < array.length; i++) {
            if (array[i].match(regex) && array[i] !== '') {
              filteredItems.push(array[i]);
            }
            if (filteredItems.length > 5) {
              resolve(filteredItems);
              return;
            }
          }
          resolve(filteredItems);
        });
      }

    function songWriterMatch (value, availableSongwriters) {
      return project.util.stringMatch(value, availableSongwriters);
    }

    return {
      validateSongwriters: validateSongwriters,
      isValidSongwriter: isValidSongwriter,
      filterSongwriters: filterSongwriters,
      songWriterMatch: songWriterMatch
    };

  })();
})();
