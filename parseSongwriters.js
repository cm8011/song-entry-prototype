var fs = require('fs');

var src = './songwriterDB.json';
var dest = './src/js/json/songwriterDB.json';

fs.readFile(src, 'utf8', function (err, data) {
  if (err) throw err;
  var arr = [];
  // var string = JSON.stringify(data);
  var json = eval(data);
  console.log(typeof json);
  json.map(function(item) {
    var name = item.displayName;
    var lastName = name.slice(0, name.indexOf(','));
    var firstName = name.slice(name.indexOf(', ') + 2, name.length);
    console.log(firstName);
    var fullName = firstName + ' ' + lastName;
    arr.push(fullName);
  });

  console.log(arr.length);

  fs.writeFile(dest, JSON.stringify(arr), function(err) {
    console.log('done');
  });
});
