/* global project */

(function () {
  'use strict';

  project.util = {

    // regexFilter: function (item, regex) {
    //   var list = [];
    //   array.forEach(function (item) {
    //     if (item.match(regex)) {
    //       list.push(item);
    //     }
    //   });
    //   return list;
    // },

    stringMatch: function (item, array) {
      var result = false;
      if (Array.isArray(array)) {
        array.forEach(function (i) {
          if (i === item) {
            result = true;
          }
        });
      }
      return result;
    },

    filter: function (term, array) {
        var regex = new RegExp(term, 'i');
        var filteredItems = [];
        array.forEach(function (item) {
          if (item.label.match(regex)) {
            filteredItems.push(item);
          }
        });
        return filteredItems;
      },

    checkUniqueCatalog: function (val, catalogs) {
      var result = true;
      catalogs.forEach(function (catalog) {
        if (catalog.catalogName === val) {
          result = false;
        }
      });
      return result;
    }
  };
})();
