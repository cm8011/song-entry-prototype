// http://stackoverflow.com/a/10233247/537998
var Handlebars = require('handlebars');
module.exports = function (context) {
  return escape(new Handlebars.SafeString(JSON.stringify(context)));
};
