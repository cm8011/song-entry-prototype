/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  project.songLyricsSub = (function () {
    function getLyrics () {
      return JSON.parse(window.localStorage.getItem('song-lyrics')) || '';
    }

    function setLyrics (data) {
      window.localStorage.setItem('song-lyrics', JSON.stringify(data));
    }

    return {
      get: getLyrics,
      set: setLyrics
    }

  })();

  react.SongLyrics = React.createClass({
    getInitialState: function () {
      return {
        activeCatalog: project.catalogSub.getActiveCatalog(),
        fieldsFull: false,
        songSessionDetails: project.songDetailsSub.getSongDetails(),
        songSessionLyrics: project.songLyricsSub.get()
      };
    },
    componentDidMount: function () {
      this.checkFields();
    },
    checkFields: function () {
      var data = this.prepData();
      var language = $('#language').val();
      var lyrics = $('#lyrics').val();
      var checked = $('#completed');
      if (
        data.language !== '' 
        && data.lyrics !== '' 
        && checked.is(':checked')
        ) {
        this.setState({ fieldsFull: true });
        project.songLyricsSub.set(this.prepData());
      } else {
        this.setState({ fieldsFull: false });
      }
    },
    prepData: function () {
      return {
        lyrics: $('#lyrics').val(),
        language: $('#language').val()
      };
    },
    getDefaults: function () {
      var data = this.state.songSessionLyrics;
      return {
        lyrics: data.lyrics || null,
        language: data.language || null
      }
    },
    formButtons: function () {
      var data = this.prepData();
      return <react.FormButtons
        dataKey='song-lyrics'
        data={data}
        nextPage={project.paths.songEntry + '/review'}
        prevPage={project.paths.songEntry + '/song-details'}
        nextEnabled={this.state.fieldsFull} />;
    },
    render: function () {
      var catalogName = this.state.activeCatalog.catalogName;
      var def = this.getDefaults();
      return (
        <div>
          <h3><i className='fa fa-file-text'></i>Enter the lyrics for: <em>{this.state.songSessionDetails.songTitle}</em>.</h3>
          <div className='song-entry-form'>
            <div className='song-lyrics'>

              <div className="lyrics-actions">
                <div className="row">

                  <div className="medium-8 columns">
                    <label>Add Lyrics Parts</label>
                    <a className="button success"><i className="fa fa-plus-circle"></i>Single Part</a>
                    <a className="button success"><i className="fa fa-plus-circle"></i>Multiple Parts</a>
                  </div>
                  <form>
                  <div className="medium-4 columns">
                    <label>Language
                      <select onChange={this.checkFields} defaultValue={def.language || 'English'} id='language'>
                        <option value='Afrikaans'>Afrikaans</option>
                        <option value='English'>English</option>
                        <option value='German'>German</option>
                        <option value='Korean'>Korean</option>
                      </select>
                    </label>
                  </div>
                  </form>
                </div>
              </div>
                  <div className="lyrics-container">
                    <div className="lyrics-part">
                      <div className="lyrics-part-heading">
                      <a className="edit-part" data-dropdown="changeHeading" aria-controls="changeHeading" aria-expanded="false" title="Change heading"><span className="lyrics-part-title">Chorus</span> <i className="fa fa-pencil"></i></a>
                      <ul id="changeHeading" className="f-dropdown" data-dropdown-content aria-hidden="true" tabIndex="-1">
                        <li><a href="#">..blank header..</a></li>
                        <li><a href="#">Bridge</a></li>
                        <li><a href="#">Chorus</a></li>
                        <li><a href="#">Descant</a></li>
                        <li><a href="#">Ending</a></li>
                        <li><a href="#">Interlude</a></li>
                        <li><a href="#">Intro</a></li>
                        <li><a href="#">Pre-Chorus</a></li>
                        <li><a href="#">Rap</a></li>
                        <li><a href="#">Spoken Words</a></li>
                        <li><a href="#">Vamp</a></li>
                        <li><a href="#">Verse</a></li>
                      </ul>
                      <a className="select-text" href="" title="Select text"><i className="fa fa-mouse-pointer"></i></a>
                      <a className="delete-part" data-reveal-id="deleteLyricPart" href="#" title="Delete part"><i className="fa fa-trash"></i></a>
                      <a className="move-part" href="" title="Move part"><i className="fa fa-bars"></i></a>
                      </div>
                      <div className="lyrics-part-container">
                        <p>Put lyrics here</p>
                      </div>
                    </div>
                    {/*<textarea onChange={this.checkFields} defaultValue={def.lyrics} id='lyrics' rows='10' placeholder="Paste or type the lyrics here..."></textarea> */}
                  </div>
                 
                <label htmlFor='completed'>
                  <input onChange={this.checkFields} id='completed' type='checkbox' />
                  &nbsp; Mark lyrics as completed
                </label>
            </div>
              {this.formButtons()}
            </div>
          </div>
      );
    }
  });

  var selector = $('[data-react="song-lyrics"]');
  if (selector.length) {
    selector.each(function () {
      ReactDOM.render(
        <react.SongLyrics />
        , $(this)[0]
      );
    });
  }
})();

          // <div className='song-entry-actions'>
          //   <a className='secondary-action'>Save &amp; Exit</a>
          //   <a className='secondary-action' href='/'>Go Back</a>
          //   <a className='button' href='/lyrics'>Save &amp; Continue</a>
          // </div>

// <a href='#'>Add another songwriter</a>
