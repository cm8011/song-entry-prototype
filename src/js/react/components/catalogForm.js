/* global React */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.CatalogForm = React.createClass({
    createDropdown: function () {
      var userCatalogs = this.props.catalogs;
      var html = [];
      if (!userCatalogs) { return null; }
      userCatalogs.map(function (item, i) {
        var option = <option key={item + i} value={JSON.stringify(item)}>{item.catalogName}</option>;
        html.push(option);
      });
      return html;
    },
    render: function () {
      console.log(this.props.activeCatalog);
      return (
        <div>
          <form className='select-catalog'>
          </form>
          <form className='add-new-catalog'>
            <div className='row'>
              <div className='medium-10 large-8 columns'>
                <div className='row collapse'>
                  <div className='small-12 columns'>
                    <label>
                      My Catalogs
                      <select
                        defaultValue={JSON.stringify(this.props.activeCatalog)}
                        className='input-alert'
                        required
                        id='catalog-name'
                        onChange={this.props.handleChange} >
                          {this.createDropdown()}
                      </select>
                      </label>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      );
    }
  });
})();
