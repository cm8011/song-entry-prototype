require('strict-version');
var fs = require('fs');
var koa = require('koa');
// var router = require('koa-router')();
var serve = require('koa-static');

var app = koa();
var port = process.env.PORT || 3001;
var dir = __dirname + '/../build';

app.use(function * pageNotFound (next) {
  yield next;
  if (this.status !== 404) return;
  this.status = 404;
  this.type = 'html';
  this.body = fs.readFileSync(dir + '/404.html');
});

app.use(serve(dir));

app.listen(port);

console.log('listening on port ' + port);
