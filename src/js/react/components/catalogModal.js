/* global React */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.CatalogModal = function () {
    return (
        <span>
          <a
            href='#'
            className='whats-this vertical-align-top'
            data-reveal-id='catalog-definition-modal'>
            what's this?
          </a>
          <div id='catalog-definition-modal' className='reveal-modal small' data-reveal aria-labelledby='modalTitle' aria-hidden='true' role='dialog'>
            <div className='modal-heading'><h1 id='modalTitle'>What's a catalog?</h1></div>
            <div className='modal-body'>
              <p className='lead'>A catalog is basically a folder that holds a set of songs.</p>
              <ul className="example">
              <li><i className="fa fa-folder-open"></i> <strong>John Smith Music</strong> <span className="help-text">&larr; The catalog.</span>
                  <ul>
                  <li>- John's First Song <span className="help-text">&larr; Songs inside the catalog.</span></li>
                    <li>- John's Second Song</li>
                  </ul>
                </li>
              </ul>
              <p>The catalog name will show up in the copyright notice that appears at the bottom of lyrics or sheet music.  For example, if John Smith names his catalog <em>John Smith Music</em>, the copyright notice for his songs would look something like this:</p>
              <blockquote>&copy; 2016 John Smith Music <span className="help-text">&larr; Copyright notice with the catalog name.</span></blockquote>
              <p>If you are registering a new catalog, you can give it any name you like, although it's very common to use your own name if you are the sole owner of the songs you are registering.</p>
            </div>
            <div className='modal-submit'>
              <button className='close-reveal-modal button apply' data-close aria-label='Close reveal'>Close</button>
            </div>
            <a className='close-reveal-modal' aria-label='Close'>&#215;</a>
          </div>
        </span>
    );
  };
})();
