/* global React*/

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.SecondaryActionWIcon = function (props) {
    return (
        <p className='subtle-text'>{props.leadingText} {props.leadingText ? <br/> : null}
          <a className='secondary-action' onClick={props.onClick}>
            <i className={'' + props.icon} aria-hidden='true'></i>
            {props.text}
          </a>
        </p>
    );
  };
})();
