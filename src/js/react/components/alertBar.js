/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.AlertBar = function (props) {
    return (
      <div onClick={props.onClick} data-alert className={'alert-box custom-alert hover-pointer ' + props.type}>
        {props.icon} {props.message} {props.link}
      </div>
    );
  };

})();
