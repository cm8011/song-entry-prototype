/* global $, React, ReactDOM */

(function () {
  'use strict';

  var project = window.project;
  var react = project.react;

  react.TextInput = function (props) {
    return (
      <label>{props.label}
        <input
        onChange={props.onChange}
        id={props.id}
        data-key={props.dataKey}
        value={props.value}
        type='text'
        placeholder={props.placeholder}
        maxLength={props.maxLength} />
      </label>
    );
  };

})();
