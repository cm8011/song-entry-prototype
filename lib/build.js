//require('strict-version');

var findit = require('findit');
var fs = require('fs-extra');
var Handlebars = require('handlebars');
var mkdirp = require('mkdirp');
var Swag = require('swag');

var slash = process.platform === 'win32' ? '\\' : '/';

var partialsFolders;
(function () {
  var partials = [];
  var path = 'src/views/partials';
  findit(path).on('directory', function (dir) {
    partials.push(dir);

  }).on('end', function () {
    console.log(partials);
    partialsFolders = partials;
    init();
  });
})();

process.project = process.project || {};
var config = process.project.config = require('./../config');
var context = process.project.context = {};

var paths = {
  views: {
    helpers: 'src/views/helpers',
    // partials: 'src/views/partials',
    partials: partialsFolders,
    pages: 'src/views/pages'
  }
};

function init () {
  try {
    walk(paths.views.helpers)
      .then(registerHbsHelpers)
      .then(() => walkPartialsArr(partialsFolders))
      .then(registerHbsPartials)
      .then(getDataForHbs)
      .then(() => walk(paths.views.pages))
      .then(renderpages)
      .then(() => process.exit())
      .catch(handleError);
  } catch (e) {
    console.log(e);
  }
}

function renderpages (files) {
  return new Promise(function (resolve, reject) {
    try {
      files.forEach(function (element, index) {
        var dest = './build/' + element.handle + '.html';
        var destDir = dest.substr(0, dest.lastIndexOf(slash));
        mkdirp.sync(destDir);
        console.log(destDir)
        context.frontendContext = {
          config: context.config
        };
        fs.writeFileSync(
          dest,
          Handlebars.compile(
            fs.readFileSync(element.full, 'utf-8')
          )(context)
        );
      });
      resolve();
    } catch (e) {
      handleError(e);
    }
  });
}

function getDataForHbs () {
  return new Promise(function (resolve, reject) {
    // context.config = config.public;
    context.config = '';
    resolve();
  });
}

function registerHbsPartials (files) {
  return new Promise(function (resolve, reject) {
    try {
      files.forEach(function (element, index) {
        Handlebars.registerPartial(
          element.handle,
          fs.readFileSync(element.full, 'utf-8')
        );
      });
      resolve();
    } catch (e) {
      handleError(e);
    }
  });
}

function registerHbsHelpers (files) {
  return new Promise(function (resolve, reject) {
    try {
      Swag.registerHelpers(Handlebars);
      files.forEach(function (element, index) {
        Handlebars.registerHelper(
          element.handle,
          require('../' + paths.views.helpers + '/' + element.rel)
        );
      });
      resolve();
    } catch (e) {
      handleError(e);
    }
  });
}

function walk (dir) {
  return new Promise(function (resolve, reject) {
    var files = [];
    findit(dir).on('file', function (file, stat) {
      var rel = file.substr(dir.length + 1);
      if (rel !== '.DS_Store') {
        files.push({
          handle: rel.substr(0, rel.lastIndexOf('.')),
          full: file,
          rel: rel
        });
      }
    }).on('error', function (err) {
      reject(err);
    }).on('end', function () {
      resolve(files);
    });
  });
}

function walkPartialsArr (arr) {
  return new Promise(function (resolve, reject) {
    var files = [];
    arr.forEach(function (dir, index) {
      findit(dir).on('file', function (file, stat) {
        var rel = file.substr(dir.length + 1);
        if (rel !== '.DS_Store') {
          files.push({
            handle: rel.substr(0, rel.lastIndexOf('.')),
            full: file,
            rel: rel
          });
        }
      }).on('error', function (err) {
        reject(err);
      }).on('end', function () {
        if (index === arr.length - 1) {
          resolve(files);
        }
      });
    });
  });
}

function handleError (args) {
  console.log('Error', args);
}
